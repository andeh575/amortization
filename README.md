# pymort

[![code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![license: MIT](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/andeh575/pymort/-/blob/master/LICENSE)

A simple `CLI` amortization tool written in Python.

## Initial setup

Install `pip`, `pipx`, `pipenv`, and `black`:

```sh
# Example is for Ubuntu/Debian

sudo apt-get install python3-pip
python3 -m pip install --user pipx

# Add the following line to your shell configuration file
export PATH=$PATH:~/.local/bin

# Install packages with `pipx` to isolate it from the system python
pipx install pipenv
pipx install black
```

Install packages into the environment:

```sh
pipenv install
```

## Development

Run the application with the following:

```sh
pipenv run python src/pymort.py
```
