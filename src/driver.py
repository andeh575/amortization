"""
Implements the main functionality of pymort
"""
from decimal import Decimal, ROUND_HALF_UP
from termcolor import colored

__author__ = "agraham"


class Driver(object):
    """
    This class implements pymort
    """

    frequency = {
        "bi-weekly": 24,
        "monthly": 12,
        "bi-monthly": 6,
        "quarterly": 4,
        "semi-annually": 2,
        "annually": 1,
    }
    cents = Decimal("0.01")

    def __init__(self, args):
        self.initial_principal = Decimal(args.principal)
        self.interest_rate = Decimal(args.interest / 100)
        self.payments = args.length * self.frequency[args.period]

    def pymort_driver(self):
        """
        Main entry for pymort
        :return: None
        """

        print(colored("Welcome to pymort", "cyan"))
        print(
            f"Principal: {self.initial_principal:>10,.2f}"
            + f"\nInterest: {self.interest_rate * 100:>10.2f}%"
            + f"\nPayments: {self.payments:>11}"
        )

        print(
            f"\nPayment: {self.periodic_payment(self.initial_principal).quantize(self.cents, ROUND_HALF_UP):.2f}"
        )

        print("\nCreating schedule...")
        self.schedule()

    def periodic_payment(self, principal):
        """
        Calculate the periodic payment
        :return: Amount
        :rtype: int
        """
        x = 1 - (1 / (1 + self.interest_rate / 12) ** self.payments)
        return ((self.interest_rate / 12) * self.initial_principal) / x

    def schedule(self):
        """
        Generate the amortization schedule
        :return: None
        """
        index = 1
        balance = self.initial_principal
        periodic_amount = self.periodic_payment(balance)

        while index <= self.payments:
            interest = balance * (self.interest_rate / 12)
            principal = periodic_amount - interest
            balance = balance - principal

            print(
                f"{index:>3}: Principal: {principal.quantize(self.cents, ROUND_HALF_UP):,.2f}; "
                + f"Interest: {interest.quantize(self.cents, ROUND_HALF_UP):,.2f}; "
                + f"Balance: {balance.quantize(self.cents, ROUND_HALF_UP):,.2f}"
            )

            index += 1
