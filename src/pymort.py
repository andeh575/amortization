"""
pymort CLI tool
"""
from argparse import ArgumentParser
from driver import Driver
from os import system

__author__ = "agraham"


def process_arguments():
    """
    Process command line arguments
    :return: parsed_arguments
    :rtype: obj
    """
    parser = ArgumentParser(prog="pymort CLI amortization tool")

    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="%(prog)s 0.1.0",
        help="Show application version",
    )
    parser.add_argument(
        "--principal",
        action="store",
        dest="principal",
        type=float,
        required=True,
        help="Amount of principal, net of initial payments (subtract any down-payments)",
    )
    parser.add_argument(
        "--interest",
        action="store",
        dest="interest",
        type=float,
        required=True,
        help="Periodic interest rate",
    )
    parser.add_argument(
        "--length",
        action="store",
        dest="length",
        type=int,
        required=True,
        help="Length of the loan (in years)",
    )
    parser.add_argument(
        "--period",
        action="store",
        default="monthly",
        dest="period",
        choices=[
            "bi-weekly",
            "monthly",
            "bi-monthly",
            "quarterly",
            "semi-annually",
            "annually",
        ],
        help="Interval for payments",
    )

    parsed_args = parser.parse_args()

    return parsed_args


# Recieve arguments from the parser
args = process_arguments()

# Instantiate the pymort tool
driver = Driver(args)

# Enable terminal colors
system("color")

# Execute the tool
driver.pymort_driver()
